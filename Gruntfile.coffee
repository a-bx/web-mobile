module.exports = (grunt) ->
  grunt.file.defaultEncoding = 'utf8';

  loadConfig = (path) ->
    glob = require("glob")
    object = {}
    key = undefined
    glob.sync("*",
      cwd: path
    ).forEach (option) ->
      key = option.replace(/\.coffee$/, "")
      object[key] = require(path + option)(grunt)
      return
    object

  config =
    pkg: grunt.file.readJSON("package.json")
  grunt.util._.extend config, loadConfig("./tasks/")
  grunt.initConfig config
  grunt.option 'dest', 'public/build'

  buildEnvironment = (env, tasks) ->
    grunt.registerTask env, 'Build', ->
      grunt.option('dest', (if env == 'test' then 'public/test' else 'public/build'))
      grunt.task.run tasks

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  buildEnvironment 'develop', [
    'concat'
    'directives'
    'coffee:compile'
    'sass'
    'cssmin'
    'copy'
    'haml'
    'clean'
  ]

  buildEnvironment 'production', [
    'concat'
    'directives'
    'coffee:compile'
    'sass'
    'cssmin'
    'copy'
    'haml'
    'uglify'
    'clean'
  ]

  buildEnvironment 'default', ['develop', 'connect:develop', 'watch']