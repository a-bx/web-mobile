module.exports = (grunt) ->
  options:
    banner: "/*! <%= pkg.name %> <%= grunt.template.today(\"yyyy-mm-dd\") %> */\n"
  build:
    src: "<%= grunt.option('dest') %>/scripts/app.js"
    dest: "<%= grunt.option('dest') %>/scripts/app.js"