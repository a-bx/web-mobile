module.exports = (grunt) ->
  vendor:
    files: [
      src: [
        'app/vendor/bower/jquery/jquery.min.js'
        'app/vendor/bower/underscore/underscore.js'
        'app/vendor/bower/backbone/backbone.js'
        'app/vendor/lib/quo.js'
        'app/vendor/lib/lungo.js'
      ]
      dest: "<%= grunt.option('dest') %>/scripts/vendor.js"
    ]
