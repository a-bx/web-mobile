module.exports = (grunt) ->
  vendor:
    files: ['app/vendor/bower/**/*.js', 'app/vendor/bower/**/*.css']
    tasks: ['concat:vendor', 'cssmin:vendor']

  scripts:
    files: ['app/scripts/**/*.coffee']
    tasks: ['directives', 'coffee:compile', 'clean:scripts']

  styles:
    files: ['app/styles/**/*.scss']
    tasks: ['sass']

  layout:
    files: ['app/index.haml']
    tasks: ['haml:layout']

  images:
    files: ['app/images/**']
    tasks: ['copy:images']

  configFiles:
    files: ['Gruntfile.coffee', 'tasks/*.coffee']
    options:
      reload: true

