module.exports = (grunt) ->

  images:
    expand: true
    flatten: true
    src: ['app/images/**']
    dest: "<%= grunt.option('dest') %>/images/"
    filter: 'isFile'