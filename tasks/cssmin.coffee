module.exports = (grunt) ->
  vendor:
    files:
      "<%= grunt.option('dest') %>/styles/vendor.css": [
        'app/styles/lib/lungo.css'
        'app/styles/lib/lungo.icon.css'
        'app/styles/lib/lungo.theme.css'
      ]
